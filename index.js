const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 2244 });

let ids = [];

const statusType = {
	READY:0,
	STARTING:1,
	RUNNING:2
};

const serverType = {
	MINIGAME:0,
	LOBBY:1,
	BUNGEE:2
};

const messageType = {
	NEWSERVER:0,
	STATUSUPDATE:1,
	STARTGAME:2,
	GAMESTARTED:3,
	READYGAMES:4,
	REMOVESERVER:5
};

const gameMode = {
	IDLE:0
}

let servers = [];

wss.on('connection', function connection(ws) {
	console.log(ws);
	ws.on('message', function incoming(message) {
		decodedMessage = JSON.parse(message);
		if (decodedMessage.messageType == messageType.NEWSERVER){
			createServer(decodedMessage,ws);
		}else if (decodedMessage.messageType == messageType.STATUSUPDATE){
			let sent = false;
			for(var i=0;i<servers.length;i++){
				const server = servers[i];
				if (server.socket == ws){
					servers[i].status = decodedMessage.status;
					servers[i].gameMode = decodedMessage.gameMode;
					sent = true;
					sendReady();
					sendBungee(JSON.stringify({
					    messageType:messageType.STATUSUPDATE,
					    id:server.id,
						status:decodedMessage.status,
						gameMode:decodedMessage.gameMode
					}));
				}
			}
			if (sent === false){
				createServer(decodedMessage,ws);
			}
		}else if (decodedMessage.messageType == messageType.STARTGAME){
			const server = getServerById(decodedMessage.id);
			if (server != null){
				startGame(server,decodedMessage.game);
			}
		}
	});
	ws.on('close', function () {
		removeServer(ws);
	});
});



function createServer(packet,ws){
	let newServer = {
		socket:ws,
		ip:packet.ip,
		status:packet.status,
		type:packet.type,
		id:generateID(),
		gameMode:gameMode.IDLE
	};
	servers[servers.length] = newServer;
	console.log(packet);
	sendReady();
	if (newServer.type != serverType.BUNGEE){
		sendBungee(JSON.stringify({
			messageType:messageType.NEWSERVER,
			server:{
				ip:newServer.ip,
				id:newServer.id,
				status:newServer.status,
				type:newServer.type,
				gameMode:newServer.gameMode
			}
		}));
	} else {
		for (let i = 0; i<servers.length; i++){
			const server = servers[i];
			if (server.type == serverType.BUNGEE){
				continue;
			}
			newServer.socket.send(JSON.stringify({
				messageType:messageType.NEWSERVER,
				server:{
					ip:server.ip,
					id:server.id,
					status:server.status,
					type:server.type,
					gameMode:newServer.gameMode
				}
			}));
		}
	}
}

function removeServer(socket){
	for(var i=0;i<servers.length;i++){
		const server = servers[i];
		if (server.socket == socket){
			servers.splice(i,1);
			ids.splice(server.id,1);
			sendBungee(JSON.stringify({
				messageType:messageType.REMOVESERVER,
				id:server.id
			}));
		}
	}
	sendReady();
}

function startGame(server, game){
	server.socket.send(JSON.stringify({
		messageType:messageType.STARTGAME,
		gameMode:game
	}));
	sendBungee(JSON.stringify({
		messageType:messageType.GAMESTARTED,
		id:server.id,
		status:server.status,
		gameMode:game
	}));
}

function sendReady(){
	readyServers = 0;
	for(var i=0;i<servers.length;i++){
		const server = servers[i];
		if (server.staus == statusType.READY){
			readyServers++;
		}
	}
	sendBungee(JSON.stringify({
		messageType:messageType.READYGAMES,
		servers:readyServers
	}));

}

function generateID(){
	for (var i=0;i<=ids.length;i++) {
		if (ids[i] != true) {
			ids[i] = true;
			return i;
		}
	}
}

function getServerBySocket(ws){
    for(var i=0;i<servers.length;i++){
        const server = servers[i];
        if (server.socket == ws){
            return server;
        }
    }
    return null;
}

function getServerById(id){
	for(var i=0;i<servers.length;i++){
        const server = servers[i];
        if (server.id == id){
            return server;
        }
    }
    return null;
}

function sendBungee(message){
	for(var i=0;i<servers.length;i++){
		const server = servers[i];
		if (server.type == serverType.BUNGEE){
			server.socket.send(message);
		}
	}
}